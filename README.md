# Veilid Internals Guide

Work in process book is available here:

https://veilid.gitlab.io/internals-book/

## Introduction

The purpose of this book is to document the Veilid architecture, design choices, and nuances of deep Veilid lore for those that want to be able to contribute to `veilid-core`.

Topics include:

* Veilid subsystems documentation
* How low level networking works
* How RPC is implemented
* Design choices and implementation details

## Other Books

For consumer-level documentation about how to write applications that use Veilid refer to the _Veilid Developer Handbook_

## Contributing

The format of this book is mdBook, see here for how to contribute:

https://rust-lang.github.io/mdBook/index.html