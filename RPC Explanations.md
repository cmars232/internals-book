# Veilid RPCs

Veilid operates on a mechanism of remote procedure calls. There is a small
group of RPCs which are provided simply by virtue of being on the network
(which is the very limited set provided by bootstrap nodes); the others are
grouped behind various "capabilities".

On the wire, these capabilities are referred to using Four Character Codes.
These are mapped in the Rust code to much longer constants:

| FourCC | Rust constant name     | Note                                      |
| ------ | ---------------------- | ----------------------------------------- |
| ROUT   | CAP_ROUTE              | [1]                                       |
| TUNL   | CAP_TUNNEL             | [2] "unstable-tunnel"                     |
| SGNL   | CAP_SIGNAL             |                                           |
| RLAY   | CAP_RELAY              | [1]                                       |
| DIAL   | CAP_VALIDATE_DIAL_INFO | [1]                                       |
| DHTV   | CAP_DHT                |                                           |
| APPM   | CAP_APPMESSAGE         |                                           |
| BLOC   | CAP_BLOCKSTORE         | [2] "unstable-blockstore"                 |

1: Not provided by the WASM build.
2: Requires compilation with the named feature.

## RPC Information per-capability

RPCs are implemented on the network as Statements, or as Question/Answer pairs. A Statement is a notification of something that the recipient wants to know, and is answered by a Return Receipt. A Question is a query; an Answer is the response.

A Question is supposed to -always- receive an Answer, unless the network itself is failing. If an Answer is not received within a reasonable time, or if an error is detected and returned, you should retransmit the Question. ((Unknown: How long is the timeout? How many retries?))

A Statement is a simple notification, with an optional return receipt.

Any RPC formed as a Question/Answer pair names both of the network packets with a Q or A suffix. Any RPC formed as a Statement names only the RPC packet, with no suffix.

For reference, here are the Rust declarations for the Questions, Answers, and Statements:

#### Questions

General:

    StatusQ(Box<RPCOperationStatusQ>)
    FindNodeQ(Box<RPCOperationFindNodeQ>)
    ReturnReceipt(Box<RPCOperationReturnReceipt>)

APPM:	
    AppCallQ(Box<RPCOperationAppCallQ>)

DHTV:	
    GetValueQ(Box<RPCOperationGetValueQ>)
    SetValueQ(Box<RPCOperationSetValueQ>)
    WatchValueQ(Box<RPCOperationWatchValueQ>)

BLOC:
    SupplyBlockQ(Box<RPCOperationSupplyBlockQ>)
    FindBlockQ(Box<RPCOperationFindBlockQ>)

TUNL:
    StartTunnelQ(Box<RPCOperationStartTunnelQ>)
    CompleteTunnelQ(Box<RPCOperationCompleteTunnelQ>)
    CancelTunnelQ(Box<RPCOperationCancelTunnelQ>)

#### Answers

General:
    StatusA(Box<RPCOperationStatusA>)
    FindNodeA(Box<RPCOperationFindNodeA>)

APPM:
    AppCallA(Box<RPCOperationAppCallA>)

DHTV:
    GetValueA(Box<RPCOperationGetValueA>)
    SetValueA(Box<RPCOperationSetValueA>)
    WatchValueA(Box<RPCOperationWatchValueA>)

BLOC:
    #[cfg(feature = "unstable-blockstore")]
    SupplyBlockA(Box<RPCOperationSupplyBlockA>)
    #[cfg(feature = "unstable-blockstore")]
    FindBlockA(Box<RPCOperationFindBlockA>)

TUNL:
    #[cfg(feature = "unstable-tunnels")]
    StartTunnelA(Box<RPCOperationStartTunnelA>)
    #[cfg(feature = "unstable-tunnels")]
    CompleteTunnelA(Box<RPCOperationCompleteTunnelA>)
    #[cfg(feature = "unstable-tunnels")]
    CancelTunnelA(Box<RPCOperationCancelTunnelA>)

#### Statements

DIAL:
    ValidateDialInfo(Box<RPCOperationValidateDialInfo>)

ROUT:
    Route(Box<RPCOperationRoute>)

DHTV:
    ValueChanged(Box<RPCOperationValueChanged>)

SGNL:
    Signal(Box<RPCOperationSignal>)
    AppMessage(Box<RPCOperationAppMessage>)

APPM:
	AppMessage(Box<RPCOperationAppMessage>)



### RPCs provided with no capabilities

All Veilid nodes are expected to respond to these RPCs.

StatusQ/StatusA
FindNodeQ/FindNodeA



### RPCs provided with ROUT

These RPCs are not supported in the WASM build.

The RPCs in ROUT are for creating safety and private routes. Advertising this means that your node will participate in routing as a safety or private route point.

Route



### RPCs provided with TUNL

The RPCs in TUNL are for forming and dissolving tunnels. Only available if veilid-core is compiled with feature "unstable-tunnel".

StartTunnelQ/StartTunnelA (initiate a tunnel through relays)
CancelTunnelQ/CancelTunnelA (Abort the formation of a tunnel, or terminate an existing tunnel)
CompleteTunnelQ/CompleteTunnelA (Notify that a tunnel has been completely formed)



### RPCs provided with SGNL

These RPCs are used to request an outbound connection from a node for direct communication. Advertising this means that your node will accept signaling requests.

Signal



### RPCs provided with RLAY

The RLAY capability is unique in that it does not provide any new RPCs, instead altering how incoming packets are handled.

Relaying is not supported by the WASM build.



### RPCs provided with DIAL

These RPCs are not supported in the WASM build.

These RPCs are related to verification of DialInfo on behalf of nodes which, for whatever reason, cannot.

ValidateDialInfo



### RPCs provided with DHTV

These RPCs are related to DHT storage and retrieval. Advertising this means that your node will participate in the DHT storage system as a storage provider.

GetValueQ/GetValueA
SetValueQ/SetValueA
WatchValueQ/WatchValueA (currently unimplemented)



### RPCs provided with APPM

These RPCs are how applications running on Veilid can directly send RPCs to each other, without involving the DHT.

AppCallQ/AppCallA (Call a procedure in a remote instance of an app, and wait for a return value)
AppMessage (Send a message to a remote instance of an app, returning immediately)



### RPCs provided with BLOC

These RPCs are only available if veilid-core is compiled with feature "unstable-blockstore".

BLOC searches, stores, retrieves, and advertises block storage. Advertising this means that your node will use and provide block storage.

FindBlockQ/FindBlockA (locate a particular entry in the block storage provided by the network)
SupplyBlockQ/SupplyBlockA (notify the network that your node is providing a certain amount of storage to the network)

